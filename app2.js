const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const jwt = require('express-jwt');
const jtoken = require('jsonwebtoken');
const delay = require('delay');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

function getRandom(max, min = 0) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

// Token Example
// var secretOrPrivateKey = "hello  BigManing"
// app.use(jwt({
//   secret: secretOrPrivateKey
// }).unless({
//   path: ['/getToken']
// }));

// app.get('/getToken', function (req, res) {
//   res.json({
//     result: 'ok',
//     token: jtoken.sign({
//       name: "BinMaing",
//       data: "============="
//     }, secretOrPrivateKey, {
//       expiresIn: 60 * 1
//     })
//   })
// });

app.get('/', function (req, res) {
  res.render('default',
    {
      title: '首頁'
    }
  );
});

app.get('/getData', function (req, res) {
  res.send(req.query.user)
});

app.get('/delay', async function (req, res) {
  let _id = req.query._id || 0;
  let _delay = getRandom(5);
  await delay(_delay * 1000);
  let result = {
    id: _id,
    delay: _delay
  };
  console.log('req: ', result);
  res.json(result);
});

/* 範例
app.post('/', function (req, res) {
  console.dir(req.body);
  res.send('OK');
});

app.get('/me', function (req, res) {
  res.send('<h1>我的FB</h1>' + 'https://www.facebook.com/witkaiy');
});

app.get('/who/:name?', function (req, res) {
  var name = req.params.name;
  res.send(name + ' 在這邊歐');
});

app.get('/who/:name?/:title?', function (req, res) {
  var name = req.params.name;
  var title = req.params.title;
  res.send('<p>名稱: ' + name + '<br>值稱: ' + title + '</p>');
});

app.get('*', function (req, res) {
  res.send('沒有東西噢');
});
*/

var server = app.listen(3000, function () {
  console.log('Fake API Server Listening on port 3000');
});   